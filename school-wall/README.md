# school-wall
02da5953682507c9837516c61aba58ef
## BUG 修复
### 未登录用户查看详情报错
```js
// 封装更新对象
const db = uniCloud.database()
const dbCmd = db.command
module.exports = {
	_before: function () {},
	/**
	 * 自定增减
	 * @param {Object} table 数据表
	 * @param {Object} attr  属性
	 * @param {Object} id    id
	 * @param {Object} num   自增减
	 */
	async operation(table, attr, id, num) {
		let obj = {}
		obj[attr] = dbCmd.inc(num)
		return await db.collection(table).doc(id).update(obj)
	},
}
```
```js
// 此方法在export 前引入
import { store, mutations } from '@/uni_modules/uni-id-pages/common/store.js';
const utilsObj = uniCloud.importObject('utilsObj', {customUI: true});

// 喜欢
async addLike() {
	// 恶意请求拦截
	let time = Date.now();
	if (time - this.likeTime < 500) {
		uni.showToast({
			title: '操作频繁',
			icon: 'none'
		});
		return;
	}
    // 无感更新
	this.detailArr.isLike ? this.detailArr.like_count-- : this.detailArr.like_count++;
	this.detailArr.isLike = !this.detailArr.isLike;
	this.likeTime = time;
	this.likeFun();
},
// 点赞操作数据库方法
async likeFun() {
    // 定义一个变量计数 判断是否时同一篇文章id用户是否是同一个
	let count = await db.collection('school_like').where(`article_id=="${this.detailId}" && user_id==$cloudEnv_uid`).count();
	if (count.result.total) {
		db.collection('school_like').where(`article_id=="${this.detailId}" && user_id==$cloudEnv_uid`).remove();
		utilsObj.operation('school_default', 'like_count', this.detailId, -1);
	} else {
		db.collection('school_like').add({
			article_id: this.detailId
	});
		utilsObj.operation('school_default', 'like_count', this.detailId, 1);
	}
},
// 获取数据
detailData() {
    let detailTemp = db.collection('school_default').where(` _id=="${this.detailId}" `).getTemp();
    let usersTemp = db.collection('uni-id-users').field('nickname,_id,avatar_file,username').getTemp();
    // 判断是否是 文章ID和用户传入的用户ID是否相等 用户ID和当前登录ID是否相等 来判断用户是否点赞
    let liskTemp = db.collection('school_like').where(`article_id=="${this.detailId}" && user_id==$cloudEnv_uid`).getTemp();
    // 定义一个零时变量把 文章表和用户表 关联
    let tempArr = [detailTemp, usersTemp];
    // 判断是否登录 登录的话把点赞表添加到零时变量
    if (store.hasLogin) tempArr.push(liskTemp);
        // 使用展开运算符把数据放进去
    db.collection(...tempArr).get({ getOne: true }).then(res => {
        this.loadState = false;
            // 定义点赞的初始值
        let isLike = false;
            // 用户登录的话判断是否点赞
        if (store.hasLogin) {
        isLike = res.result.data._id.school_like.length ? true : false;
        }
        // 把用户是否点赞的数据放到数组渲染
        res.result.data.isLike = isLike;
        this.detailArr = res.result.data;
        this.user_id = res.result.data.user_id[0];
    });
}
```
### 首页点赞不高亮
```js
db.collection(indexTemp, usersTemp)
	// 模糊搜索
	.where(`${new RegExp(this.keyword, 'i')}.test(content)`)
	.orderBy(this.navList[this.navAction].type, 'desc')
	.get()
	.then(async res => {
		// 定义一个临时变量存每个文章的id
		let idArr = [];
		let resDataArr = res.result.data;
		// 变量数组将文章id写入临时变量
		resDataArr.forEach(item => {
			idArr.push(item._id);
		});
		// 用户登录的话等待一个求情 拿到点赞文章的id和点赞的用户id
		let likeRes = await db
			.collection('school_like')
			.where({
				article_id: dbCmd.in(idArr),
				user_id: uniCloud.getCurrentUserInfo().uid
			}).get();
		// 遍历等待的求情 拿到用户点赞的文章索引
		likeRes.result.data.forEach(item => {
			// 返回索引
			let findIndex = resDataArr.findIndex(find => {
				// 判断用户点击文章id和全部文章id是否相等
				return item.article_id == find._id;
			});
			resDataArr[findIndex].isLike = true;
		});
		// 没有登录的话执行
		this.dataList = resDataArr;
		this.loadState = false;
```

