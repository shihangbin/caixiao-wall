"use weex:vue";
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/*!*****************************************!*\
  !*** E:/uni-app/school-wall/pages.json ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor
    return this.then(function (value) {
      return promise.resolve(callback()).then(function () {
        return value
      })
    }, function (reason) {
      return promise.resolve(callback()).then(function () {
        throw reason
      })
    })
  }
}
if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
}


if(uni.restoreGlobal){
  uni.restoreGlobal(weex,plus,setTimeout,clearTimeout,setInterval,clearInterval)
}
__definePage('pages/index/index',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/index/index.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/chat/chat',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/chat/chat.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/user',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/user.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/index/edit/edit',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/index/edit/edit.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/chat/exchange/exchange',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/chat/exchange/exchange.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/index/details/details',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/index/details/details.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/deactivate/deactivate',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/deactivate/deactivate.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/userinfo',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/userinfo.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/bind-mobile/bind-mobile',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/bind-mobile/bind-mobile.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/cropImage/cropImage',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/cropImage/cropImage.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/login/login-withoutpwd',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/login/login-withoutpwd.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/login/login-withpwd',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/login/login-withpwd.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/login/login-smscode',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/login/login-smscode.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/register/register',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/register/register.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/register/register-by-email',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/register/register-by-email.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/retrieve/retrieve',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/retrieve/retrieve.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/retrieve/retrieve-by-email',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/retrieve/retrieve-by-email.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/common/webview/webview',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/common/webview/webview.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/change_pwd/change_pwd',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/change_pwd/change_pwd.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/register/register-admin',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/register/register-admin.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/school_default/list',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/school_default/list.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/school_like/list',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/school_like/list.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/school_comment/list',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/school_comment/list.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-feedback/pages/opendb-feedback/opendb-feedback',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-feedback/pages/opendb-feedback/opendb-feedback.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-feedback/pages/opendb-feedback/list',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-feedback/pages/opendb-feedback/list.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-feedback/pages/opendb-feedback/detail',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-feedback/pages/opendb-feedback/detail.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/index/revise/revise',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/index/revise/revise.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/regards/regards',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/regards/regards.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/chat/detail/detail',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/chat/detail/detail.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/set-pwd/set-pwd',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/set-pwd/set-pwd.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('uni_modules/uni-id-pages/pages/userinfo/realname-verify/realname-verify',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'uni_modules/uni-id-pages/pages/userinfo/realname-verify/realname-verify.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/office/office',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/office/office.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/user/login/login',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/user/login/login.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/chat/edit/edit',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/chat/edit/edit.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/office/detail/detail',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/office/detail/detail.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/office/edit/edit',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/office/edit/edit.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})
__definePage('pages/office/exchange/exchange',function(){return Vue.extend(__webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module 'pages/office/exchange/exchange.vue?mpType=page'"); e.code = 'MODULE_NOT_FOUND'; throw e; }())).default)})


/***/ })
/******/ ]);