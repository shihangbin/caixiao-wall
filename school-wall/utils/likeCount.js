// 点赞操作数据库方法
const db = uniCloud.database();
const utilsObj = uniCloud.importObject('utilsObj', {
	customUI: true
});
import { store, mutations } from '@/uni_modules/uni-id-pages/common/store.js';

export async function likeFun(likeID) {
	let count = await db
		.collection('school_like')
		.where(`article_id=="${likeID}" && user_id==$cloudEnv_uid`)
		.count();

	if (count.result.total) {
		db.collection('school_like')
			.where(`article_id=="${likeID}" && user_id==$cloudEnv_uid`)
			.remove();
		utilsObj.operation('school_default', 'like_count', likeID, -1);
	} else {
		db.collection('school_like').add({
			article_id: likeID
		});
		utilsObj.operation('school_default', 'like_count', likeID, 1);
	}
}
